# examadcprog3

To [International version](#international-version)


# Dutch version
International version: ./readme_int.md

***Dutch***
Workshop tentamen Advanced C Programming

***Toelichting op dit tentamen***

Bij dit tentamen wordt de theoretische en praktische kennis van onderwerpen behorende bij de OWE Advanced C Programming getoetst.

Toetsing vindt plaats door het binnen de beschikbare tentamentijd van 3 lesuren drie compleet uitgewerkte opdrachten te maken. In ieder van deze programma's behandelen we een deel van de stof die behoort tot de leeruitkomsten van Advanced C Programming

Bij dit tentamen mag je gebruik maken van:

- Het boek Advanced C programming
- Eventuele aantekeningen
- Een laptop met het beschikbaar gestelde Ubuntu 20.x image

***Gedurende het tentamen mag je geen verbinding maken met het internet, dus tijdens je tentamen staan de netwerkverbindingen uit.***

***Dit is een individueel tentamen, je mag gedurende het tentamen op geen enkele wijze communiceren met anderen (zowel studenten als buitenstaanders).***

Je ontwerpt, ontwikkelt en realiseert de gevraagde programma functionaliteit en onderdelen. 
Opdrachten worden uitgewerkt onder Linux op de in de les gebruikte Ubuntu 20.x image

- Als er om functies wordt gevraagd worden deze correct en werkend gecodeerd.
- Er wordt gecodeerd volgens de geldende code-style 
- Alle code is compileerbaar er zijn geen errors of warnings
- De functie wordt opgenomen in de daarvoor bestemde (of te maken) module.
- Als je een deel van een opdracht niet kunt maken, gebruik dan een zelf ingestelde waarde of functie om toch met de vervolgopdrachten verder te gaan.
  Voorbeelden:

Als opdracht 1a niet lukt, kies dan bij opdracht 1b zelf een waarde om de opdracht te maken

**Inleveren:**

Maak een zip van de complete directory met opgaven en lever deze in via HANDIN. Dit kan door gebruik te maken van *firefox* en het adres handin.han.nl. Je kunt het inleveren bij:

Deeltijd: Tentamen Advanced C programmeren

Voltijd: Lab Advanced C

De zipfile maak als volgt:

cp -r ADC-tentamen ADC-tentamen.save

zip -r ADC-tentamen.zip ADC-tentamen-<studentnummer>

(let op zip op linux heeft eerst de destination en dan hetgeen je wilt "zippen")


# International version
Workshop tentamen Advanced C Programming

***Explanation for this exam***

In this exam, the theoretical and practical knowledge of subjects belonging to the OWE Advanced C Programming is tested.

Testing takes place by completing three fully worked out assignments within the available exam time of 3 teaching hours. In each of these programs, we cover some of the material that is part of the learning outcomes of Advanced C ProgrammingBij dit tentamen mag je gebruik maken van:

- The book Advanced C programming
- Any notes
- A laptop with the provided Ubuntu 20.x image

***During the exam you are not allowed to connect to the internet, so the network connections are switched off during the exam.***

***This is an individual exam, during the exam you are not allowed to communicate in any way with others (both students and outsiders).***

You design, develop and realize the requested program functionality and components.

Assignments are worked out under Linux on the Ubuntu 20.x image used in the lesson

- If functions are requested, they are coded correctly and working.
- Encoding is done according to the applicable code style
- All code is compilable and there are no errors or warnings
- The function is included in the appropriate (or to be created) module.
- If you can't complete part of an assignment, use a custom value or function to continue with subsequent assignments.

Example:

If assignment 1.1 fails, choose a value yourself at assignment 1.2 to create the assignment

**HandIn:**

Make a zip of the complete directory with exercises and hand it in via HANDIN. This can be done by using firefox and the address handin.han.nl. You can hand it in at:

Programmeren 3 - Lab - Advanced C

De zipfile maak als volgt:

cp -r ADC-tentamen ADC-tentamen.save

zip -r ADC-tentamen.zip ADC-tentamen-<studentnumber>

(note that zip on linux has the destination first and then what you want to "zip")


==

