#include <stdio.h>
#include <stdlib.h>

#define SIZE 5

typedef struct sensorNode
{
    int senseNumber;
    int *values;            //sensor uses an array with SIZE integer values
} sensorNode_t;

void funcSensorNode(sensorNode_t *destination, const sensorNode_t source, const int size);
void printSensorNode(const sensorNode_t node, const int size);

int main(void)
{
    sensorNode_t node1 = {1, NULL};
    sensorNode_t node2 = {2, NULL};
    node1.values = (int *)malloc(sizeof(int) * SIZE);
    for (int i = 0; i < SIZE; i++)
    {
       node1.values[i] = i* 10;
    }
    printSensorNode(node1, SIZE);
    funcSensorNode(&node2, node1, SIZE);
//    printSensorNode(node2, SIZE);          // Uncomment this part after implementing funcSensorNode()
}

void funcSensorNode(sensorNode_t *destination, const sensorNode_t source, int size)
{


}

void printSensorNode(sensorNode_t node, int size)
{
    printf("Values of sensor %d:\n", node.senseNumber);
    for (int i = 0; i < size; i++)
        printf("%d\n", node.values[i]);
}
